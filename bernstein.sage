# -*- mode: python; -*-

from scipy.special import comb

def bernstein_basico(k, n):
    return comb(n, k, exact=True) * pow(t, k) * pow(1-t, n-k)


def bernstein(f, n):
    return sum([f(k/n) * bernstein_basico(k, n) for k in range(n+1)])
