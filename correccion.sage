# -*- mode:python; -*-

import numpy as np
from math import prod

def correccion(r, l):
    '''Construye los polinomios básicos de corrección (r puntos, l+1 derivadas).

    Devuelve una matriz de tamaño r × (l+1) cuya entrada (i,k) es P_{ik},
    para i = 0..r-1 y para k = 0..l.
    '''
    ret = np.empty([r, l+1], dtype=object)

    # guarda (t - t_i)^k para cada par (i, k)
    potencias_monomios = np.empty([r, l+2], dtype=object)
    for i in range(r):
        potencias_monomios[i, 0] = 1
        for k in range(1, l+2):
            potencias_monomios[i, k] = (t - ti[i]) * potencias_monomios[i, k-1]


    # guarda (t_j - t_i)^{l+1} para cada par (i, j)
    potencias_ti_tj = np.empty([r, r], dtype=object)
    for i in range(r-1):
        for j in range(i+1, r):
            potencias_ti_tj[i, j] = pow(ti[j] - ti[i], l+1)
            potencias_ti_tj[j, i] = pow(-1, l+1) * potencias_ti_tj[i, j]


    # guarda k! para cada k
    factoriales = np.empty(l+1, dtype=object)
    factoriales[0] = 1
    for k in range(1, l+1):
        factoriales[k] = k * factoriales[k-1]


    # guarda la constante c_{ik} (sin el factor k!) para cada i
    c = np.empty(r, dtype=object)
    for i in range(r):
        aux = prod([potencias_ti_tj[i, j] for j in range(r) if j != i])
        c[i] = 1 / pow(-aux, l+1)


    # calcula los r * (l+1) productos
    for i in range(r):
        for k in range(0, l+1):
            aux = prod([potencias_monomios[i, l+1] - potencias_ti_tj[i, j]
                        for j in range(r) if j != i])
            ret[i, k] = c[i] / factoriales[k] * \
                        potencias_monomios[i, k] * pow(aux, l+1)

    return ret
