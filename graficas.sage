# -*- mode:python; -*-

import numpy as np
import matplotlib.pyplot as plt

def dibujar_correccion_basicos():
    '''Dibuja los r * (l+1) polinomios básicos de corrección. '''
    P = correccion(r, l)  # r puntos, l+1 derivadas
    ts = [k/n_puntos for k in range(n_puntos+1)]

    _, axs = plt.subplots(r, l+1)
    for i in range(r):
        for k in range(l+1):
            xs = [P[i, k](t) for t in ts]
            axs[i][k].plot(ts, xs)

    plt.show()


def graficar(n):
    '''Dibuja la gráfica principal para grado n.

    Incluye los polígonos que forman el conjunto semialgebraico, el camino gamma
    y la aproximación de Bernstein.
    '''
    plt.gca().set_aspect('equal')  # usar la misma escala para los dos ejes

    dibujar_poligonos()
    dibujar_gamma()
    dibujar_bernstein(n)
    # dibujar_correccion(n)

    plt.show()


def dibujar_poligonos():
    S1 = plt.Polygon(np.array([[-1, 1], [-1, 0], [0, 0]]), alpha=0.7)
    S2 = plt.Polygon(np.array([[0, 0], [0, 1], [1, 1], [1, 0]]), alpha=0.7)
    S3 = plt.Polygon(np.array([[1, 1], [2, 1], [2, 0]]), alpha=0.7)

    plt.gca().add_patch(S1)
    plt.gca().add_patch(S2)
    plt.gca().add_patch(S3)


def dibujar_gamma():
    xs = [x(k/n_puntos) for k in range(n_puntos+1)]
    ys = [y(k/n_puntos) for k in range(n_puntos+1)]

    plt.plot(xs, ys, color='r')

    # para mostrar los puntos de control:
    # plt.scatter([x(t) for t in ti], [y(t) for t in ti], color='g')


def dibujar_bernstein(n):
    xx = bernstein(x, n)
    yy = bernstein(y, n)

    xs = [xx(k/n_puntos) for k in range(n_puntos+1)]
    ys = [yy(k/n_puntos) for k in range(n_puntos+1)]

    plt.plot(xs, ys, color='y')

    # para mostrar los polinomios de Bernstein en los puntos de control:
    # plt.scatter([xx(t) for t in ti], [yy(t) for t in ti], color='g')


def dibujar_correccion(n):
    P = correccion(r, l)
    xx = bernstein(x, n)
    yy = bernstein(y, n)

    gx, gy = xx, yy  # partimos de la aproximación de Bernstein
    for i in range(r):
        for k in range(l+1):
            # sumamos las correcciones a cada coordenada:
            bx = derivadas_gamma[i, k, 0] - xx.derivative(t, k)(ti[i])
            by = derivadas_gamma[i, k, 1] - yy.derivative(t, k)(ti[i])
            gx += bx * P[i, k]
            gy += by * P[i, k]

    xs = [gx(k/n_puntos) for k in range(n_puntos+1)]
    ys = [gy(k/n_puntos) for k in range(n_puntos+1)]

    plt.plot(xs, ys, color='g')
