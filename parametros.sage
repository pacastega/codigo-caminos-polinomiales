# -*- mode:python; -*-

n = 200  # grado de los polinomios de Bernstein

r = 5  # número de puntos de control
l = 3  # número de derivadas a tener en cuenta
ti = [k/(r-1) for k in range(r)]  # tiempos de control

t = polygen(QQ, 't')  # generador del anillo de polinomios Q[t]

n_puntos = 100  # tamaño de los muestreos (por ejemplo, para las gráficas)
