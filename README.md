# Caminos polinomiales

Este repositorio contiene el código desarrollado para ayudar en los cálculos de mi Trabajo de Fin de Grado en Matemáticas. Se ha usado [SageMath](https://www.sagemath.org/) para realizar los cálculos simbólicos con polinomios.

Para poder usar el código, es suficiente ejecutar la siguiente instrucción desde el _prompt_ de Sage:

```python
attach("parametros.sage", "caminos.sage", "bernstein.sage", \
       "correccion.sage", "graficas.sage", "test.sage")
```
