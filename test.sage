# -*- mode:python; -*-

import numpy as np
import matplotlib.pyplot as plt

def test_b_ik(n):
    '''Muestra todos los b_ik para un grado n dado. '''
    xx = bernstein(x, n)
    yy = bernstein(y, n)

    print('----- x -----')
    for i in range(r):
        for k in range(l+1):
            b_ik_x = (derivadas_gamma[i, k, 0] - xx.derivative(t, k)(ti[i])).n()
            print(f'{b_ik_x:+.3e}', end='\t')  # imprimir con 3 decimales
        print()

    print('----- y -----')
    for i in range(r):
        for k in range(l+1):
            b_ik_y = (derivadas_gamma[i, k, 1] - yy.derivative(t, k)(ti[i])).n()
            print(f'{b_ik_y:+.3e}', end='\t')  # imprimir con 3 decimales
        print()


def max_P_ik():
    '''Imprime los máximos de (un muestreo de) los polinomios de corrección. '''
    P = correccion(r, l)
    for i in range(r):
        for k in range(l+1):
            max = np.max([P[i, k](j/n_puntos) for j in range(n_puntos+1)])
            print(f'{max:>20.3f}', end='\t')
        print()


def test_correccion():
    '''Calcula las derivadas de los P_{ik} y las evalúa en cada tiempo t_i.

    Es útil para verificar que están bien calculados.
    '''
    P = correccion(r, l)
    for i in range(r):
        for k in range(l+1):
            print([[P[i, k].derivative(t, orden)(ti[punto])
                    for orden in range(l+1)] for punto in range(r)])
