# -*- mode: python; -*-

import numpy as np
from math import sin, cos, pi
import matplotlib.pyplot as plt

def transf_afin_M(M, x, v):
    '''Devuelve M*x + v, es decir, la imagen de x vía la transformación afín
    [1 0]
    [v M]
    '''
    return np.matmul(M, np.array(x, dtype=object)) + np.array(v, dtype=object)


def transf_afin(c, s, x, v):
    '''Devuelve M*x + v, es decir, la imagen de x vía la transformación afín
    [1 0]
    [v M]
    siendo M la matriz de rotación
    [c -s]
    [s  c]
    '''
    M = np.array([[c, -s], [s, c]], dtype=object)
    return transf_afin_M(M, x, v)


def esta_en(x, intervalo):
    (a, b) = intervalo
    return a <= x <= b


delta = 1 / 24

t1 = 1/8
tau1, theta1 = t1-delta, t1+delta
c1, s1 = 5/13, -12/13  # coseno, seno (phi1 ~ -3*pi/8)
k1 = 4/10
def beta1(t):
    delta1 = 13/20  # 0.65
    t = (t-t1) * delta1 / delta

    x = np.array([t**2, k1 * t**3], dtype=object)
    v = np.array([-1, 1], dtype=object)

    return transf_afin(c1, s1, x, v)


def sigma1(t):
    a, b = beta1(theta1), lambda1(xi1)
    t = np.array([(t-theta1) / delta], dtype=object)
    return (1-t)*a + t*b


s1_ = 2/8
xi1, zeta1 = s1_-delta, s1_+delta
def lambda1(t):
    rho1 = 3/10  # 0.3
    t = (t-s1_) * rho1 / delta
    return np.array([t, t**2], dtype=object)


def sigma2(t):
    a, b = lambda1(zeta1), beta2(tau2)
    t = np.array([(t-zeta1) / delta], dtype=object)
    return (1-t)*a + t*b


t2 = 3/8
tau2, theta2 = t2-delta, t2+delta
def beta2(t):
    delta2 = 1/10  # 0.1
    t = (t-t2) * delta2 / delta
    return np.array([t+65/100, 17/100-t^2], dtype=object)


def sigma3(t):
    a, b = beta2(theta2), beta3(tau3)
    t = np.array([(t-theta2) / delta], dtype=object)
    return (1-t)*a + t*b


t3 = 4/8
tau3, theta3 = t3-delta, t3+delta
c3, s3 = -119/169, 120/169  # coseno, seno (phi2 ~ 3*pi/4)
k3 = -4/10
def beta3(t):
    delta3 = 1/2
    t = (t-t3) * delta3 / delta

    x = np.array([t**2, k3 * t**3], dtype=object)
    v = np.array([1, 0], dtype=object)

    return transf_afin(c3, s3, x, v)


def sigma4(t):
    a, b = beta3(theta3), beta4(tau4)
    t = np.array([(t-theta3) / delta], dtype=object)
    return (1-t)*a + t*b


t4 = 5/8
tau4, theta4 = t4-delta, t4+delta
def beta4(t):
    delta4 = 1/10  # 0.1
    t = (t-t4) * delta4 / delta
    return np.array([81/100+t^2, 69/100+t], dtype=object)


def sigma5(t):
    a, b = beta4(theta4), lambda2(xi2)
    t = np.array([(t-theta4) / delta], dtype=object)
    return (1-t)*a + t*b


s2 = 6/8
xi2, zeta2 = s2-delta, s2+delta
def lambda2(t):
    rho2 = 1/12  # 0.083
    t = (t-s2) * rho2 / delta
    return np.array([t+1, 1-6*t**2], dtype=object)


def sigma6(t):
    a, b = lambda2(zeta2), beta5(tau5)
    t = np.array([(t-zeta2) / delta], dtype=object)
    return (1-t)*a + t*b


t5 = 7/8
tau5, theta5 = t5-delta, t5+delta
c5, s5 = -5/13, 12/13  # coseno, seno (phi3 ~ 5*pi/8)
k5 = -4/10
def beta5(t):
    delta5 = 13/20  # 0.65
    t = (t-t5) * delta5 / delta

    x = np.array([t**2, k5 * t**3], dtype=object)
    v = np.array([2, 0], dtype=object)

    return transf_afin(c5, s5, x, v)


# tiempos que definen los intervalos en los que el camino se comporta distinto
tiempos = [tau1, theta1, xi1,
           zeta1, tau2, theta2, tau3, theta3, tau4, theta4, xi2,
           zeta2, tau5, theta5]
# funciones en cada uno de esos intervalos
caminos = [beta1, sigma1, lambda1,
           sigma2, beta2, sigma3, beta3, sigma4, beta4, sigma5, lambda2,
           sigma6, beta5]

m = len(tiempos)


def gamma(t):
    # reparametrizar evitando los trozos a la izda. de t1 y a la dcha. de t5:
    t = t * (t5 - t1) + t1

    # reparametrizar conservando los trozos sobrantes:
    # t = t * (theta3 - tau1) + tau1

    for i in range(m-1):
        if esta_en(t, (tiempos[i], tiempos[i+1])):
            return caminos[i](t)


def crear_derivadas():
    '''Crea un array con las derivadas de gamma en los tiempos prefijados.

    Están calculadas de forma exacta (a mano), en lugar de numéricamente. Esto
    es posible porque, por construcción, gamma es derivable (de hecho, de clase
    infinito) en dichos tiempos porque coincide con las respectivas beta_i o
    lambda_i.
    '''
    ret = np.empty([r, l+1, 2], dtype=object)

    ret[0,:,:] = [[-1,1], [0,0], [2*c1,2*s1], [-6*k1*s1,6*k1*c1]]  # beta1
    ret[1,:,:] = [[ 0,0], [1,0], [0,2],       [0,0]]               # lambda1
    ret[2,:,:] = [[ 1/2,3/20], [1,0], [0,-2], [0,0]]               # beta2
    ret[3,:,:] = [[ 1,0], [0,0], [2*c3,2*s3], [-6*k3*s3,6*k3*c3]]  # beta3
    ret[4,:,:] = [[ 7/10,13/20], [0,1], [2,0],[0,0]]               # beta4
    ret[5,:,:] = [[ 1,1], [1,0], [0,-2],      [0,0]]               # lambda2
    ret[6,:,:] = [[ 2,0], [0,0], [2*c5,2*s5], [-6*k5*s5,6*k5*c5]]  # beta5

    return ret


derivadas_gamma = crear_derivadas()
x = lambda t: gamma(t)[0]
y = lambda t: gamma(t)[1]
